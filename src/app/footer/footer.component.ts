import { Component, OnInit } from '@angular/core';
import { TretmaniService } from '../tretmani.service';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(private service : TretmaniService) { }
  tretmani;
  ngOnInit(): void {
    this.service.dohvatiSveTretmane().subscribe(
      Response => {
        this.tretmani = Response;
      },
      error => {
        alert("Error: " + error)
      }
    )
  }

}
