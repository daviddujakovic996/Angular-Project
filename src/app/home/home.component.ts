import { Component, OnInit } from '@angular/core';
import { SlideInOutAnimation } from '../animation';
import { DohvatiZaposleneService } from '../dohvati-zaposlene.service';
import { TretmaniService } from '../tretmani.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [SlideInOutAnimation]
})
export class HomeComponent implements OnInit {
  animationState = 'out';

  toggleShowDiv(divName: string) {
    if (divName === 'divA') {
      console.log(this.animationState);
      this.animationState = this.animationState === 'out' ? 'in' : 'out';
      console.log(this.animationState);
    }
  }
  constructor(private services:DohvatiZaposleneService,private service : TretmaniService) { }
  zaposleni;
  tretmani;
  bookingForm : FormGroup;
    ngOnInit() {
      this.services.dohvatiSveZaposlene().subscribe(
        Response => {
          this.zaposleni = Response;
        },
        error => {
          alert("Error: " + error)
        }
      )
      this.service.dohvatiSveTretmane().subscribe(
        Response => {
          this.tretmani = Response;
        },
        error => {
          alert("Error: " + error)
        }
      )
      this.bookingForm = new FormGroup({
        fullName : new FormControl("", [Validators.required, Validators.pattern('^[A-ZŠĐČĆŽ][a-zšđčćž]{1,12}\\s[A-ZŠĐČĆŽ][a-zšđčćž]{1,12}$')]),
        phoneNumber : new FormControl("", [Validators.required, Validators.pattern('^[0-9]{9,}$')]),
        service : new FormControl("", [Validators.required]),
        chiropractic : new FormControl("", [Validators.required]),
        date : new FormControl("", [Validators.required]),
        time : new FormControl("", [Validators.required])
      })
    }
    uspeh=false;
    onFormSubmit() {
      this.uspeh=true;
      this.bookingForm.reset();
    }
}
