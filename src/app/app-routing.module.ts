import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TretmaniComponent } from './tretmani/tretmani.component';
import { TretmanComponent } from './tretman/tretman.component';
import { ZaposleniComponent } from './zaposleni/zaposleni.component';
import { KontaktComponent } from './kontakt/kontakt.component';

const routes: Routes = [
  {
    path : "",
    component : HomeComponent
  },
  {
    path : "tretmani",
    component : TretmaniComponent
  },
  {
    path : "tretmani/:id",
    component : TretmanComponent
  },
  {
    path : "zaposleni",
    component : ZaposleniComponent
  },
  {
    path : "kontakt",
    component : KontaktComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
