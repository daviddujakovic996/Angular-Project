import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { paths } from './paths';

@Injectable({
  providedIn: 'root'
})
export class DohvatiZaposleneService {

  constructor(private service:HttpClient) { }

  dohvatiSveZaposlene(){
    let zaposleni=this.service.get(paths.zaposleni);
    return zaposleni;
  }
}
