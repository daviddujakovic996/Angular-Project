import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TretmanComponent } from './tretman.component';

describe('TretmanComponent', () => {
  let component: TretmanComponent;
  let fixture: ComponentFixture<TretmanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TretmanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TretmanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
