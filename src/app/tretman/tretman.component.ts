import { Component, OnInit } from '@angular/core';
import { TretmaniService } from '../tretmani.service';
import { HttpClient } from '@angular/common/http';
import {Router, ActivatedRoute, Params } from '@angular/router';
@Component({
  selector: 'app-tretman',
  templateUrl: './tretman.component.html',
  styleUrls: ['./tretman.component.css']
})
export class TretmanComponent implements OnInit {

  id
  tretmani
  tretman
  constructor(private service: TretmaniService, private http: HttpClient, private route: ActivatedRoute) { 
    this.route.params.subscribe((params : Params) => {
      this.id = +this.route.snapshot.params["id"]
    })
  }

  ngOnInit() {
    this.service.dohvatiSveTretmane().subscribe(
      Response => {
        this.tretmani = Response;
        for(let i=0; i<this.tretmani.length; i++){
          if(this.tretmani[i].id == this.id){
            this.tretman = this.tretmani[i]
            console.log(this.tretman)
            break;
          }
        }
      },
      error => {
        alert("Error: " + error)
      }
    )
  }

}
