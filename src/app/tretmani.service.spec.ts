import { TestBed } from '@angular/core/testing';

import { TretmaniService } from './tretmani.service';

describe('TretmaniService', () => {
  let service: TretmaniService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TretmaniService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
