import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { HttpClient,HttpClientModule } from '@angular/common/http';
import { TretmaniComponent } from './tretmani/tretmani.component';
import { ShortenPipe } from './shorten.pipe';
import { TretmanComponent } from './tretman/tretman.component';
import { ZaposleniComponent } from './zaposleni/zaposleni.component';
import { KontaktComponent } from './kontakt/kontakt.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    FooterComponent,
    TretmaniComponent,
    ShortenPipe,
    TretmanComponent,
    ZaposleniComponent,
    KontaktComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
