import { Component, OnInit } from '@angular/core';
import { MenuService } from '../menu.service';
import { Menu } from '../menu';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private services:MenuService) { }
  public menu:Menu[];
  ngOnInit() {
    this.services.getMenu().subscribe(
      (Response:Menu[] )=> {
        this.menu = Response;
      },
      error => {
        alert("Error: " + error)
      }
    )
  }
}
