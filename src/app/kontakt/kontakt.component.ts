import { Component, OnInit } from '@angular/core';
import { transition, style, animate, trigger } from '@angular/animations';
import { FormGroup, FormControl, Validators } from '@angular/forms';

const enterTransition = transition(':enter', [
  style({
    opacity: 0
  }),
  animate('1s ease-in', style({
    opacity: 1
  }))
]);
const leaveTrans = transition(':leave', [
  style({
    opacity: 1
  }),
  animate('1s ease-out', style({
    opacity: 0
  }))
])

const fadeIn = trigger('fadeIn', [
  enterTransition
]);

const fadeOut = trigger('fadeOut', [
  leaveTrans
]);
@Component({
  selector: 'app-kontakt',
  templateUrl: './kontakt.component.html',
  styleUrls: ['./kontakt.component.css'],
  animations: [
    fadeIn,
    fadeOut
  ]
})

export class KontaktComponent implements OnInit {
  
  

 
  contactForm : FormGroup;


  constructor() { }

  ngOnInit() {
    this.contactForm = new FormGroup({
      fullName : new FormControl("", [Validators.required, Validators.pattern('^[A-ZŠĐČĆŽ][a-zšđčćž]{1,12}\\s[A-ZŠĐČĆŽ][a-zšđčćž]{1,12}$')]),
      email : new FormControl("", [Validators.required, Validators.email]),
      subject : new FormControl("", [Validators.required, Validators.minLength(5), Validators.maxLength(20)]),
      message : new FormControl("", [Validators.required, Validators.minLength(5), Validators.maxLength(300)])
    })
  }



 
  show = false;
  uspeh=false;
  onClick() {
    this.show = true;
  }
  onSave(){
    this.show = false; 
    this.uspeh=true;
  }
  onFormSubmit() {
    
    this.contactForm.reset();
   
  
   
  }
 
}





