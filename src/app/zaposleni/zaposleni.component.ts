import { Component, OnInit } from '@angular/core';
import { DohvatiZaposleneService } from '../dohvati-zaposlene.service';
@Component({
  selector: 'app-zaposleni',
  templateUrl: './zaposleni.component.html',
  styleUrls: ['./zaposleni.component.css']
})
export class ZaposleniComponent implements OnInit {

  constructor(private services:DohvatiZaposleneService) { }
  zaposleni;
 
    ngOnInit() {
      this.services.dohvatiSveZaposlene().subscribe(
        Response => {
          this.zaposleni = Response;
        },
        error => {
          alert("Error: " + error)
        }
      )
    }
  

}
