import { TestBed } from '@angular/core/testing';

import { DohvatiZaposleneService } from './dohvati-zaposlene.service';

describe('DohvatiZaposleneService', () => {
  let service: DohvatiZaposleneService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DohvatiZaposleneService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
