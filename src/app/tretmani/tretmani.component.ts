import { Component, OnInit } from '@angular/core';
import { TretmaniService } from '../tretmani.service';

@Component({
  selector: 'app-tretmani',
  templateUrl: './tretmani.component.html',
  styleUrls: ['./tretmani.component.css']
})
export class TretmaniComponent implements OnInit {

  tretmani;
  constructor(private service : TretmaniService) { }

  ngOnInit() {
    this.service.dohvatiSveTretmane().subscribe(
      Response => {
        this.tretmani = Response;
      },
      error => {
        alert("Error: " + error)
      }
    )
  }
  

}
