import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TretmaniComponent } from './tretmani.component';

describe('TretmaniComponent', () => {
  let component: TretmaniComponent;
  let fixture: ComponentFixture<TretmaniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TretmaniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TretmaniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
