import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { paths } from './paths';

@Injectable({
  providedIn: 'root'
})
export class TretmaniService {
  constructor(private service : HttpClient) { }

  dohvatiSveTretmane(){
    var products = this.service.get(paths.tretmani)
    return products;
  }
 
 
}
